/*jshint esversion: 6 */

// RexEx to extract info from Google CDN URL
const googleParser = /ajax\.googleapis\.com\/ajax\/libs\/(.+?)\/(.+?)\/(.+)/;

// Replace Google URLs with cdnjs
function deGoogle(url) {
	// Exract lib name, version and path from URL
	let groups = googleParser.exec(url);
	let [, lib, version, path] = groups;

	// TODO: Some library names don't match (ex. threejs => three.js)
	// This could be solved by using their [API](https://cdnjs.com/api) to search for a match

	// Construct the cdnjs URL
	let newUrl = `https://cdnjs.cloudflare.com/ajax/libs/${lib}/${version}/${path}`;

	console.debug(`[deGoogler] ${url} => ${newUrl}`);
	return newUrl;
}

// List of patterns the script will intercept
// These need to be in the permissions manifest too
// TODO: Take this directly from the manifest
const PATTERNS = ["*://ajax.googleapis.com/*"];

// Only intercept scripts and styles
const TYPES = ["script", "stylesheet"];

function redirect(requestDetails) {
	return {
		redirectUrl: deGoogle(requestDetails.url)
	};
}

// Intercept Google resources and redirect them
browser.webRequest.onBeforeRequest.addListener(redirect, {
	urls: PATTERNS,
	types: TYPES
}, ["blocking"]);
